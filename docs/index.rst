.. jeocatalog documentation master file, created by
   sphinx-quickstart on Mon Aug 26 09:18:36 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to jeocatalog's documentation!
======================================

A python client for JEODPP's `catalogue <https://jeodpp.jrc.ec.europa.eu/apps/catalogue/>`_

.. toctree::
   :maxdepth: 2
   :caption: Table of Contents:

   installation
   usage
   api
   changelog


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
