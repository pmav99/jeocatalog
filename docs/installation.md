# Installation

For now the easiest way to install is to download the `*.zip` file from the Gitlab UI
and then install it with:

    pip install /path/to/source-code.zip
