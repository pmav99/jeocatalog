# jeocatalog

A python client for JEODPP's [catalogue](https://jeodpp.jrc.ec.europa.eu/apps/catalogue/)

**Requires** Python 3.6+!!!

## Installation

For now, the easiest way to install the package is to download the zip file from the
gitlab UI and install it with pip. E.g.:

```
pip install --user /path/to/master.zip
```

Usage within a Virtualenv is *strongly* suggested.

## Command line interface

Installing the package will add a new command, `jeocatalog` which prints a JSON file on
the terminal.

Examples of usage:

```
jeocatalog --help
jeocatalog full --help
jeocatalog s2 --help
```

### Examples

- Any product's metadata

```
jeocatalog full \
    --start 2018-08-01 \
    --stop 2018-08-03 \
    --product_type S2MSI1C \
    --wkt 'POLYGON((22.604370117187493 36.73007950707844,22.604370117187493 36.73007950707844,22.58514404296874 36.71026542647846,22.58514404296874 36.71026542647846,22.58514404296874 36.70806354647627,22.58514404296874 36.70586160338115,22.587890624999993 36.703659597194545,22.589263916015618 36.70255857044239,22.604370117187493 36.73007950707844))'
```

- Sentinel 2 metadata

```
jeocatalog s2 \
    --start 2018-08-01 \
    --stop 2018-08-03 \
    --columns bands,crs,filepath \
    --wkt 'POLYGON((22.604370117187493 36.73007950707844,22.604370117187493 36.73007950707844,22.58514404296874 36.71026542647846,22.58514404296874 36.71026542647846,22.58514404296874 36.70806354647627,22.58514404296874 36.70586160338115,22.587890624999993 36.703659597194545,22.589263916015618 36.70255857044239,22.604370117187493 36.73007950707844))'
```

## Python library

You can also use it as a library from your Python scripts, by calling functions that
will return dictionaries:

``` python

from jeocatalog import get_full_metadata, get_S2_metadata

wkt = "POLYGON((22.604370117187493 36.73007950707844,22.604370117187493 36.73007950707844,22.58514404296874 36.71026542647846,22.58514404296874 36.71026542647846,22.58514404296874 36.70806354647627,22.58514404296874 36.70586160338115,22.587890624999993 36.703659597194545,22.589263916015618 36.70255857044239,22.604370117187493 36.73007950707844))"

full_metadata = get_full_metadata(
    wkt=wkt,
    start="2019-01-01",
    stop="2019-01-03",
    product_type="S2MSI1C",
)

S2_metadata = get_S2_metadata(
    wkt=wkt,
    start="2019-01-01",
    stop="2019-01-03",
    columns="crs,bands,filepath",
)
```
