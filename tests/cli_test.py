import json
import traceback


import click.testing
import pytest

import jeocatalog.cli

from .utils import get_data, dict_to_cli_options


@pytest.mark.parametrize(
    "pretty", [
        pytest.param("--pretty", id="pretty"),
        pytest.param("--no-pretty", id="no_pretty"),
    ]
)
@pytest.mark.parametrize(
    "data_file", [
        pytest.param("full_all_products.json", id="full all products"),
        pytest.param("s2_all_products.json", id="S2 all products"),
        pytest.param("s2_L1C.json", id="S2 L1C"),
        pytest.param("s2_L2A.json", id="S2 L2A"),
    ]
)
def test_cli(requests_mock, data_file, pretty):
    # get data and mock
    subcmd, endpoint, request_data, response_data = get_data(data_file)
    requests_mock.get(endpoint, json=response_data)
    # test the cli
    runner = click.testing.CliRunner()
    args = [subcmd, *dict_to_cli_options(request_data)] + [pretty]
    result = runner.invoke(jeocatalog.cli.cli, args)
    cli_output = json.loads(result.output)
    # print(traceback.print_exception(*result.exc_info))
    assert result.exit_code == 0
    assert cli_output == response_data
