import pytest
import requests

import jeocatalog

from .utils import get_data


_DISPATCHER = {
    "full": jeocatalog.get_full_metadata,
    "s2": jeocatalog.get_S2_metadata,
}


@pytest.mark.parametrize(
    "data_file", [
        pytest.param("full_all_products.json", id="full all products"),
        pytest.param("s2_all_products.json", id="S2 all products"),
        pytest.param("s2_L1C.json", id="S2 L1C"),
        pytest.param("s2_L2A.json", id="S2 L2A"),
    ]
)
def test_api(requests_mock, data_file):
    subcmd, endpoint, request_data, response_data = get_data(data_file)
    function = _DISPATCHER[subcmd]
    requests_mock.get(endpoint, json=response_data)
    assert response_data == function(**request_data)
