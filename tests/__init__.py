import pathlib


TESTDIR = pathlib.Path(__file__).parent.expanduser().resolve()
ROOTDIR = TESTDIR.parent
DATADIR = TESTDIR / "data"
