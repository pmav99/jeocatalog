import datetime

import pytest

from jeocatalog.base import ParametersBase


class TestParameterBase(object):

    def test_start_greater_than_stop_raises(self):
        today = datetime.date.today()
        yesterday = today - datetime.timedelta(days=1)
        with pytest.raises(ValueError) as exc:
            ParametersBase(
                start=today,
                stop=yesterday,
                wkt="POINT(0 0)",
            )
        assert "Start date after stop date" in str(exc.value)
        assert str(today) in str(exc.value)
        assert str(yesterday) in str(exc.value)

    def test_invalid_wkt_raises(self):
        with pytest.raises(ValueError) as exc:
            ParametersBase(
                start="2018-01-01",
                stop="2018-01-03",
                wkt="gibberish",
            )
        assert "WKT is not valid" in str(exc.value)
