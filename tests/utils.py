import itertools
import json

from . import DATADIR


def get_data(filename):
    path = DATADIR / filename
    with path.open("r") as fd:
        data = json.load(fd)
    return data["subcmd"], data["endpoint"], data["request"], data["response"]


def dict_to_cli_options(data):
    keys = [f"--{key}" for key in data.keys()]
    return itertools.chain.from_iterable(zip(keys, data.values()))


def get_fixture_as_cli_cmd(subcmd, request_data):
    cli_options = " ".join(dict_to_cli_options(request_data))
    return f"jeocatalog {subcmd} {cli_options}"
