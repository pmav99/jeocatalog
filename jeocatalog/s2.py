import datetime
import enum
import typing

import pydantic
import requests

from .base import ParametersBase


__all__ = ["S2_ProductType", "S2_METADATA", "get_S2_metadata"]


S2_URL = "https://jeodpp.jrc.ec.europa.eu/services/catalogue/collection/sentinel2"
S2_METADATA = frozenset(
    {
        "bands",
        "acquisitionStartTime",
        "acquisitionStopTime",
        "archivingDateTime",
        "bqaFiles",
        "cloudCoverPercentage",
        "crs",
        "dataType",
        "filepath",
        "illuminationAzimuthAngle",
        "illuminationZenithAngle",
        "instrumentName",
        "instrumentShortName",
        "metadataFiles",
        "nodataValue",
        "orbitDirection",
        "orbitNumber",
        "platformFamilyName",
        "platformFamilyShortName",
        "platformName",
        "platformSerialIdentifier",
        "platformShortName",
        "processingCenter",
        "processingDateTime",
        "processingLevel",
        "productInformation_size",
        "productType",
        "qidataFiles",
        "receivingStation",
        "relativeOrbitNumber",
        "sensorType",
        "title",
        "uuid",
    }
)


class S2_ProductType(enum.Enum):
    """
    An ``enum.Enum`` with the available S2 products.

    """

    S2MSI1C = "S2MSI1C"
    S2MSI2A = "S2MSI2A"
    S2MSI2Ap = "S2MSI2Ap"

    @classmethod
    def _has_value(cls, value):
        return value in cls._value2member_map_


class S2MetadataParameters(ParametersBase):
    product_type: str = None
    columns: str = None

    @pydantic.validator("columns")
    def is_columns_valid(cls, columns_value, values):
        if "," in columns_value:
            columns = columns_value.split(",")
        else:
            columns = [columns_value]
        keys = set(columns)
        if not keys.issubset(S2_METADATA):
            raise ValueError(f"Keys are not valid: {keys.difference(S2_METADATA)}")
        return columns

    @pydantic.validator("product_type")
    def is_product_type_valid(cls, product_type_value, values):
        if not S2_ProductType._has_value(product_type_value):
            msg = f"Key <{product_type_value}> is invalid. Valid values: {S2_ProductType._member_names_}"
            raise ValueError(msg)
        else:
            return S2_ProductType._value2member_map_[product_type_value]

    def _to_payload(self):
        payload = super()._to_payload()
        payload.update(
            {
                "productType": self.product_type.value if self.product_type else None,
                "returning": ",".join(self.columns) if self.columns else None,
            }
        )
        # remove empty keys
        payload = {k: v for k, v in payload.items() if v}
        return payload


def get_S2_metadata(
    wkt: str = None,
    start: typing.Union[str, datetime.date] = None,
    stop: typing.Union[str, datetime.date] = None,
    max_clouds: typing.Union[str, float] = 75,
    product_type: typing.Union[str, S2_ProductType, None] = None,
    columns: typing.List[str] = None,
) -> dict:
    """
    Return a dictionary with metadata for the selected Sentinel 2 products

    Parameters
    ----------

    wkt:
        A Well Known Text (WKT) describing a region.
    start:
        The acquisition start date. Results include the start date.
    stop:
        The acquisition stop date. Results exclude the stop date.
    max_clouds:
        The maximum cloud coverage percentage. Defaults to 75.
    product_type:
        The abbreviation of the product type. If none are specified then all product
        types are returned.
    columns:
        If not specified, then the full metadata will be returned.

    """
    payload = S2MetadataParameters(**locals())._to_payload()
    response = requests.get(S2_URL, params=payload)
    response.raise_for_status()
    data = response.json()
    if data[0]["results"]["total"] == 0:
        print(response.url)
    return data
