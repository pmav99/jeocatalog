import datetime
import enum
import typing

import pydantic
import requests

from .base import ParametersBase


__all__ = ["ProductType", "get_full_metadata"]


URL = "https://jeodpp.jrc.ec.europa.eu/services/catalogue/dataset"


class ProductType(enum.Enum):
    """
    An ``enum.Enum`` with the available products.

    """

    SLC = "SLC"
    GRD = "GRD"
    S2MSI1C = "S2MSI1C"
    S2MSI2A = "S2MSI2A"
    S2MSI2Ap = "S2MSI2Ap"

    @classmethod
    def _has_value(cls, value):
        return value in cls._value2member_map_


class FullMetadataParameters(ParametersBase):
    product_type: str = None

    @pydantic.validator("product_type")
    def is_product_type_valid(cls, product_type_value, values):
        if not ProductType._has_value(product_type_value):
            msg = f"Key <{product_type_value}> is invalid. Valid values: {ProductType._member_names_}"
            raise ValueError(msg)
        else:
            return ProductType._value2member_map_[product_type_value]

    def _to_payload(self):
        payload = super()._to_payload()
        payload.update(
            {"productType": self.product_type.value if self.product_type else None}
        )
        # remove empty keys
        payload = {k: v for k, v in payload.items() if v}
        return payload


def get_full_metadata(
    wkt: str = None,
    start: typing.Union[str, datetime.date] = None,
    stop: typing.Union[str, datetime.date] = None,
    max_clouds: typing.Union[str, float] = 75,
    product_type: typing.Union[str, ProductType, None] = None,
) -> dict:
    """
    Return a dictionary with metadata for the selected products

    Parameters
    ----------
    wkt:
        A Well Known Text (WKT) describing a region.
    start:
        The acquisition start date. Results include the start date.
    stop:
        The acquisition stop date. Results exclude the stop date.
    max_clouds:
        The maximum cloud coverage percentage. Defaults to 75.
    product_type:
        The abbreviation of the product type. If none are specified then all product
        types are returned.

    """
    payload = FullMetadataParameters(**locals())._to_payload()
    response = requests.get(URL, params=payload)
    response.raise_for_status()
    data = response.json()
    if data[0]["results"]["total"] == 0:
        print(response.url)
    return data
