import datetime

import pydantic
import shapely.wkt


class ParametersBase(pydantic.BaseModel):
    start: datetime.date = None
    stop: datetime.date = None
    max_clouds: float = 75
    wkt: str = None

    @pydantic.validator("stop")
    def ensure_that_stop_after_start(cls, stop_value, values):
        start_value = values.get("start")
        if start_value:
            if start_value >= stop_value:
                raise ValueError(
                    f"Start date after stop date: start {start_value} - stop {stop_value}"
                )
        return stop_value

    @pydantic.validator("wkt")
    def wkt_is_valid(cls, wkt_value, values):
        try:
            shapely.wkt.loads(wkt_value)
        except shapely.errors.WKTReadingError:
            raise ValueError("WKT is not valid: %s" % wkt_value)
        return wkt_value

    @pydantic.validator("max_clouds")
    def _max_clouds_is_valid(cls, max_clouds_value, values):
        if not (0 <= max_clouds_value <= 100):
            raise ValueError(
                f"max_clouds must be 0 <= value <= 100, not: {max_clouds_value}"
            )
        return max_clouds_value

    def _to_payload(self):
        payload = {
            "footprint": self.wkt if self.wkt else None,
            "acquisitionStartTime": f">={self.start}" if self.start else None,
            "acquisitionStopTime": f"<{self.stop}" if self.stop else None,
            "cloudCoverPercentage": f"<{self.max_clouds}",
        }
        # remove empty keys
        payload = {k: v for k, v in payload.items() if v}
        return payload
