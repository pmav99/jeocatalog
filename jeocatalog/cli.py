import json
import pprint

import click

from jeocatalog import get_full_metadata, ProductType
from jeocatalog import get_S2_metadata, S2_METADATA, S2_ProductType


@click.group()
def cli():
    pass


@cli.command()  # @cli, not @click!
@click.option('--wkt', required=True, default=None, help='Footprint of the AOI')
@click.option('--start', required=False, default=None, help='Acquisition Start time')
@click.option('--stop', required=False, default=None, help='Acquisition Stop time')
@click.option('--max_clouds', required=False, default=75, help='Maximum cloud coverage percentage')
@click.option('--product_type', required=False, type=click.Choice(ProductType._member_names_))
@click.option('--pretty/--no-pretty', default=True, is_flag=True, help='Pretty print output')
def full(wkt, start, stop, max_clouds, product_type, pretty):
    """ Query the catalogue and retrieve the full metadata. """
    data = get_full_metadata(wkt, start, stop, max_clouds, product_type)
    if pretty:
        output = json.dumps(data, indent=2, sort_keys=True)
    else:
        output = json.dumps(data)
    click.echo(output)



@cli.command()
@click.option('--wkt', required=True, default=None, help='Footprint of the AOI')
@click.option('--start', required=False, default=None, help='Acquisition Start time')
@click.option('--stop', required=False, default=None, help='Acquisition Stop time')
@click.option('--max_clouds', required=False, default=75, help='Maximum cloud coverage percentage')
@click.option('--product_type', required=False, type=click.Choice(S2_ProductType._member_names_))
@click.option('--columns', required=False, default=None, help='Return just the selected columns')
@click.option('--pretty/--no-pretty', default=True, is_flag=True, help='Pretty print output')
def s2(wkt, start, stop, max_clouds, product_type, columns, pretty):
    """ Query the catalogue for S2 metadata and optionally specify the returned fields """
    data = get_S2_metadata(wkt, start, stop, max_clouds, product_type, columns)
    if pretty:
        output = json.dumps(data, indent=2, sort_keys=True)
    else:
        output = json.dumps(data)
    click.echo(output)
